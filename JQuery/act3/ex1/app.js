let a, b, c;
$("#sum").on("click", function () {
    a = parseInt($("#op1").val());
    b = parseInt($("#op2").val());
    c = a + b;
    $("#resultat").html(c);
});
$("#res").on("click", function () {
    a = parseInt($("#op1").val());
    b = parseInt($("#op2").val());
    c = a - b;
    $("#resultat").html(c);
});
$("#mul").on("click", function () {
    a = parseInt($("#op1").val());
    b = parseInt($("#op2").val());
    c = a * b;
    $("#resultat").html(c);
});
$("#div").on("click", function () {
    a = parseInt($("#op1").val());
    b = parseInt($("#op2").val());
    c = a / b;
    $("#resultat").html(c);
});