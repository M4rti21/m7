const body = document.getElementsByTagName('body')[0];
body.classList.add("bg-secondary");
body.classList.add("text-dark");

function canviEstil() {
    if (document.getElementById("selector").checked === true) {
        body.classList.add("bg-dark");
        body.classList.add("text-light");

        if (body.classList.contains("bg-secondary")) {
            body.classList.remove("bg-secondary");
        }
        if (body.classList.contains("text-dark")) {
            body.classList.remove("text-dark");
        }

    } else {
        body.classList.add("bg-secondary");
        body.classList.add("text-dark");

        if (body.classList.contains("bg-dark")) {
            body.classList.remove("bg-dark");
        }
        if (body.classList.contains("text-light")) {
            body.classList.remove("text-light");
        }

    }
}