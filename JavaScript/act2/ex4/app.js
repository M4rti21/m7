function pot() {
    let r, g, b;
    let cnc = 0;
    document.getElementById("llista").innerHTML = "";
    for (let i = 0; i < 10; i++) {
        document.getElementById("llista").appendChild(document.createElement("li"));
        r = Math.floor(Math.random() * 255);
        g = Math.floor(Math.random() * 255);
        b = Math.floor(Math.random() * 255);
        document.getElementsByTagName("li")[i].className = "list-group-item";
        document.getElementsByTagName("li")[i].innerHTML = "Text numero " + i;
        document.getElementsByTagName("li")[i].style.backgroundColor = "rgb(" + r + "," + g + "," + b + ")";
        if (r + g + b < 300) {
            document.getElementsByTagName("li")[i].style.color = "white";
        }
    }
}