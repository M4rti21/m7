let elms = document.getElementsByTagName("strong");
let body = document.getElementsByTagName("body")[0];

function highlight() {
    for (let i = 0; i < elms.length; i++) {
        elms[i].style.color = "#008000";
        elms[i].style.fontSize = "1.2em";
    }
    body.style.color = "#ffffff";
    body.style.backgroundColor = "#000000";

}
function return_normal() {
    for (let i = 0; i < elms.length; i++) {
        elms[i].style.color = "#000000";
        elms[i].style.fontSize = "1em";
    }
    body.style.color = "#000000";
    body.style.backgroundColor = "#ffffff";
}