let as = [{
    titol: 'Camí al futur',
    autor: 'Bill Gates',
    id: 1254
},
    {
        autor: 'Walter Isaacson',
        titol: 'Steve Jobs',
        id: 4264
    },
    {
        titol: 'L’ocell de la revolta',
        autor: 'Suzanne Collins',
        id: 3245
    }];
as.sort((a, b) => (a.id < b.id) ? 1 : (a.id === b.id) ? ((a.id < b.id) ? 1 : -1) : -1);
console.log("objecte ordenat de major a menor:");
console.log(as);