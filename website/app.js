let i = 0;
let arr = document.getElementsByClassName("arround");

for (let l = 0; l < arr.length; l++) {
    arr[l].style.animationDelay = l + "s";
}

function start() {
    for (i = 0; i < 50; i++) {
        makeStar();
    }
}

function makeStar() {
    let elem;
    let rnd;
    document.getElementById("stars").prepend(document.createElement("div"));
    elem = document.getElementById("stars").getElementsByTagName("div")[0];
    rnd = Math.floor(Math.random() * 10) + 1;
    elem.style.height = rnd + "px";
    elem.style.width = rnd + "px";
    rnd = Math.floor(Math.random() * window.innerHeight);
    elem.style.top = rnd + "px";
    rnd = Math.floor(Math.random() * 50) + 20;
    elem.style.opacity = rnd + "%";
    rnd = Math.random() * 6;
    elem.style.animationDelay = rnd + "s";
    rnd = Math.random() * 8 + 1;
    elem.style.animationDuration = rnd + "s";
    rnd = Math.floor(Math.random() * 2);
    elem.style.zIndex = rnd;
    setTimeout(() => {
        elem.remove();
        }, 15000)
    setTimeout(makeStar, 6000);
}