let timerstop = true;
let timersec = 0;
let playing = true;
let mouse = true;
let bombs;
const playArea = document.getElementById("play-area");
const cells = playArea.getElementsByTagName("div");
let timerkeep = true;
let rnd;
const sweep = document.getElementById("sweep");
modeM();
create();
clock();

function rest() {
    document.getElementById("reset").style.borderStyle = "inset";
    setTimeout(create, 100);
}

function clock() {
    let date = new Date();
    let h = date.getHours(); // 0 - 23
    let m = date.getMinutes(); // 0 - 59
    let s = date.getSeconds(); // 0 - 59
    let session = "AM";

    if (h === 0) {
        h = 12;
    }

    if (h > 12) {
        h = h - 12;
        session = "PM";
    }

    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;

    let time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;

    setTimeout(clock, 1000);

}

function timer() {
    timerstop = false;
    if (timerkeep) {
        timersec++;
        document.getElementById("timer").innerHTML = timersec;
        setTimeout(timer, 1000);
    }
}

function create() {
    timerstop = true;
    timersec = 0;
    playing = true;
    mouse = true;
    timerkeep = true;
    modeM();
    document.getElementById("reset").style.borderStyle = "outset";
    timersec = 0;
    document.getElementById("timer").innerHTML = timersec;
    timerkeep = false;
    timerstop = true;
    timersec = -1;
    playing = true;
    timerkeep = true;
    document.getElementById("play-area").innerHTML = "";
    document.getElementById("mode").style.backgroundImage = 'url("pointer.png")';
    let close;
    bombs = 0;
    for (let i = 0; i < 240; i++) {
        playArea.appendChild(document.createElement("div"));
        if (i % 16 === 15 || i % 16 === 0 || i < 16 || i > (20 * 12) - 16) {
            cells[i].classList.add("wall");
        } else {
            rnd = Math.floor(Math.random() * 7);
            if (rnd === 2) {
                cells[i].classList.add("bomb");
                bombs++;
            } else {
                cells[i].classList.add("safe");
            }
            cells[i].classList.add("cell", "unselected");
            cells[i].style.fontSize = "0px";
            cells[i].addEventListener("click", function () {
                show(i);
            });
        }
        document.getElementById("bombs-left").innerHTML = bombs;
    }
    for (let i = 0; i < cells.length; i++) {
        close = 0;
        if (cells[i].classList.contains("safe")) {
            if (i % 16 === 15 || i % 16 === 0) {
            } else {
                if (i >= 1) {
                    if (cells[i - 1].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i >= 15) {
                    if (cells[i - 15].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i >= 16) {
                    if (cells[i - 16].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i >= 17) {
                    if (cells[i - 17].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i <= 20 * 12 - 15) {
                    if (cells[i + 15].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i <= 20 * 12 - 16) {
                    if (cells[i + 16].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i <= 20 * 12 - 17) {
                    if (cells[i + 17].classList.contains("bomb")) {
                        close++;
                    }
                }
                if (i <= 20 * 12 - 1) {
                    if (cells[i + 1].classList.contains("bomb")) {
                        close++;
                    }
                }
            }
            if (close > 0) {
                cells[i].innerHTML = close;
                if (close === 1) {
                    cells[i].style.color = "#0000ff"
                } else if (close === 2) {
                    cells[i].style.color = "#007700"
                } else if (close === 3) {
                    cells[i].style.color = "#ff0000"
                } else if (close === 4) {
                    cells[i].style.color = "#cc11cc";
                } else if (close === 5) {
                    cells[i].style.color = "#964B00"
                } else if (close === 6) {
                    cells[i].style.color = "#00cccc"
                } else if (close === 7) {
                    cells[i].style.color = "#000000"
                } else if (close === 8) {
                    cells[i].style.color = "#555555"
                }
                cells[i].classList.add("num");
            } else {
                cells[i].classList.add("notnum");
            }
        }
    }
}

function modeM() {
    mouse = true;
    document.getElementById("flags").style.borderStyle = "outset";
    document.getElementById("mode").style.backgroundImage = 'url("pointer.png")';
    document.getElementById("mouse").style.borderStyle = "inset";
}

function modeF() {
    mouse = false;
    document.getElementById("mouse").style.borderStyle = "outset";
    document.getElementById("mode").style.backgroundImage = 'url("flag.png")';
    document.getElementById("flags").style.borderStyle = "inset";
}

function show(num) {
    let a = num;
    if (playing && cells[a].classList.contains("unselected")) {
        if (timerstop) {
            timerkeep = true;
            timersec = -1;
            timer();
        }
        if (cells[a].classList.contains("flag")) {
            cells[a].style.backgroundImage = '';
            cells[a].classList.remove("flag");
            cells[a].classList.add("unselected");
            bombs++;
            document.getElementById("bombs-left").innerHTML = bombs;
        }
        else if (mouse) {
            if (cells[a].classList.contains("bomb")) {
                for (let a = 0; a < cells.length; a++) {
                    if (cells[a].classList.contains("bomb")) {
                        cells[a].style.backgroundImage = 'url("bomb.png")';
                    }
                }
                cells[a].style.backgroundImage = 'url("bombcross.png")';
                playing = false;
                timerkeep = false;

            }
            else if (cells[a].classList.contains("notnum")) {
                cells[a].style.borderStyle = "solid";
                cells[a].style.borderColor = "#bbbbbb";
                cells[a].style.fontSize = "20px";
                cells[a].classList.remove("unselected");
                if (a >= 1) {
                    if (cells[a - 1].classList.contains("safe")) {
                        cells[a - 1].style.fontSize = "20px";
                        cells[a - 1].style.borderStyle = "solid";
                        cells[a - 1].style.borderColor = "#bbbbbb";
                        cells[a - 1].classList.remove("unselected");
                    }
                }
                if (a >= 15) {
                    if (cells[a - 15].classList.contains("safe")) {
                        cells[a - 15].style.fontSize = "20px";
                        cells[a - 15].style.borderStyle = "solid";
                        cells[a - 15].style.borderColor = "#bbbbbb";
                        cells[a - 15].classList.remove("unselected");
                    }
                }
                if (a >= 16) {
                    if (cells[a - 16].classList.contains("safe")) {
                        cells[a - 16].style.fontSize = "20px";
                        cells[a - 16].style.borderStyle = "solid";
                        cells[a - 16].style.borderColor = "#bbbbbb";
                        cells[a - 16].classList.remove("unselected");
                    }
                }
                if (a >= 17) {
                    if (cells[a - 17].classList.contains("safe")) {
                        cells[a - 17].style.fontSize = "20px";
                        cells[a - 17].style.borderStyle = "solid";
                        cells[a - 17].style.borderColor = "#bbbbbb";
                        cells[a - 17].classList.remove("unselected");
                    }
                }
                if (a <= 20 * 12 - 15) {
                    if (cells[a + 15].classList.contains("safe")) {
                        cells[a + 15].style.fontSize = "20px";
                        cells[a + 15].style.borderStyle = "solid";
                        cells[a + 15].style.borderColor = "#bbbbbb";
                        cells[a + 15].classList.remove("unselected");
                    }
                }
                if (a <= 20 * 12 - 16) {
                    if (cells[a + 16].classList.contains("safe")) {
                        cells[a + 16].style.fontSize = "20px";
                        cells[a + 16].style.borderStyle = "solid";
                        cells[a + 16].style.borderColor = "#bbbbbb";
                        cells[a + 16].classList.remove("unselected");
                    }
                }
                if (a <= 20 * 12 - 17) {
                    if (cells[a + 17].classList.contains("safe")) {
                        cells[a + 17].style.fontSize = "20px";
                        cells[a + 17].style.borderStyle = "solid";
                        cells[a + 17].style.borderColor = "#bbbbbb";
                        cells[a + 17].classList.remove("unselected");
                    }
                }
                if (a <= 20 * 12 - 1) {
                    if (cells[a + 1].classList.contains("safe")) {
                        cells[a + 1].style.fontSize = "20px";
                        cells[a + 1].style.borderStyle = "solid";
                        cells[a + 1].style.borderColor = "#bbbbbb";
                        cells[a + 1].classList.remove("unselected");
                    }
                }
            }
            else {
                cells[a].style.borderStyle = "solid";
                cells[a].style.borderColor = "#bbbbbb";
                cells[a].style.fontSize = "20px";
                cells[a].classList.remove("unselected");
            }
        }
        else {
            cells[a].style.backgroundImage = 'url("flag.png")';
            cells[a].classList.add("flag");
            cells[a].classList.remove("unselected");
            bombs--;
            document.getElementById("bombs-left").innerHTML = bombs;
        }
        let cnt = 0;
        for (let i = 0; i < 240; i++) {
            if (cells[i].classList.contains("unselected")) {
                cnt++
            }
        }
        if (cnt === 0) {
            timerkeep = false;
            playing = false;
        }
    }
}

function closeApp() {
    document.getElementById("game").style.display = "none";
    sweep.classList.remove("element-active");
    sweep.classList.add("element-closed");
}

function minimize() {
    document.getElementById("game").style.display = "none";
    sweep.classList.remove("element-active");
    sweep.classList.add("element-minimized");
}

function minesweep() {
    if (sweep.classList.contains("element-closed")) {
        history.go(0);
    } else if (sweep.classList.contains("element-minimized")) {
        document.getElementById("game").style.display = "block";
        sweep.classList.remove("element-minimized");
        sweep.classList.add("element-active");
    } else {
        minimize();
    }
}

dragElement(document.getElementById("game"));

function dragElement(elmnt) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById("header")) {
        document.getElementById("header").onmousedown = dragMouseDown;
    } else {
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}